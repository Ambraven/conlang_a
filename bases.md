Intentions
==========

- Pas d'exception!
- une particule marquant chaque groupe grammatical (sujet/verbe/complément) (peut être ignoré si le sens est suffisamment clair)
- pas de genre par défaut. Le genre peut être précisé par une particule. Particule Femme/Homme/Neutre.
- terminaison différentes mais systématiques pour nom/verbe/adjectif/adverbe
- Pas d'exception!!!
- les verbes se déclinent en passé/présent/futur. C'est tout.

Notes
-----
notes d'AliceinCrystalTokyo : par contre, si tu n'as que trois temps, je pense que tu vas devoir à un moment combler certaines nuances de sens avec peut être des auxiliaires, ou des particules (comme en grec, par exemple, où un futur est indiqué par la particule θα précédée du présent, tout simplement), mais ça tu verras au fur et à mesure que ta langue se développera

pour référence : https://witches.town/@Ambraven/5531177

phonologie 
==========

just use romanji? sounds boring but it seems balanced between easy to prononce and complet enough.
romanji légèrement modifié :

voyelles : a e(é) i o u(ou)

consonnes : k g(gu) s z j(ji) c(sh) t d n h(h/f) b p m y r(r/l) w(oua)

toutes combinaisons consonne+voyelle possibles.
Possibilité de coller deux consonnes ou deux voyelles sauf en début et fin de mot (les terminaisons ne compte pas).
pas de doublement de consonnes ou voyelles.

construction des mots
=====================

* nom : finis en e (é)
* adjectif : fini en i
* verbe : fini en a
* adverbe : fini en u

- pronom : pronom personnel comme un nom, donc termie en e. Fonctionne comme adjectif pour le pocessif, donc terminaison en i.

| pronom      |  pocessif       |
|:-----------:|:---------------:|
|me -> je/moi | mi -> mon/ma/mes|
|ye -> tu/toi | yi -> ton/ta/tes|
|re -> iel    | ri -> son/sa/ses|

- marque du pluriel : r (prononcé r/l potentiellement avec une variation selon l'accent.)

| pronom      |  pocessif       |
|:-----------------:|:------------------:|
|mer -> nous        | mir -> notres / nos|
|yer -> vous        | yir -> votres / vos|
|rer -> iels/elleux | rir -> leur / leurs|

- Pas d'articles, le pronom "s'accorde" avec le pocesseur seulement. Le pluriel de ce qui est possédé est marqué sur le nom de ce qui est pocédé.

- Les mots commence par une consonne, sauf les particules. Les coordinations finissent par une consonne.

- Les adjectifs se placent après le mot qu'ils qualifient.

- Le verbe être : ha (prononcé fa/ha)

me ha meme -> je suis parent. ( les particules devant le sujet et devant parent sont omises parce que le sens est évident )

ha meme -> je suis parent. ( la première personne du singulier peux etre omise également )

les premières particules
========================

* o : sujet
* a : verbe (je ne sais pas encore dans quel cas elle sera utile, mais ça arrivera probablement...)
* e : complément

e meme a me ha -> je suis parent.

* u : destinataire d'une action
* i : indication de temps ou de lieu.

yane geba e tahahe u meme i jibe i ye -> une personne donne une pomme au parent à une maison une nuit. 

| vocabulaire |
|:-----------|
|yane : une personne|
|geba : donner|
|tahahe : une pomme|
|meme : un parent|
|jibe : une maison|
|ye : une nuit|

construction du masculin/féminin/neutre
=======================================

* -ot- Masculin -> yanote : un homme
* -os- féminin  -> yanose : une femme
* -om- Neutre   -> yanome : une personne non binaire

Passé/futur
===========

* za- : futur -> zaha : je serais
* da- : passé -> daha : j'étais

* ma- : action continue -> mageba : je suis en train de donner

* sa- : conditionnel    -> sageba : (si ...) je donnerais

sa toujours en premier, puis za/da, puis ma.

sadamageba -> (si ...) j'aurais eu donné ?

conjonctions
============

|Conjonctions|
|:----------|
|per: mais|
|kam: ou|
|yan: et|
|ted: donc|
|zer: or|
|ran: ni|
|kah: car|
|doyoh: comme|
|Tok: lorsque|
|ber: puisque|
|kan: quand|
|nam: si|
|wer: où|

nam tenera e tahahe, sasuma e re. -> si j'avais une pomme, je la mangerais.

|vocabulaire|
|:---------|
|tenera: avoir|
|suma: manger|

association de mot
==================

on peut lier 2 mots avec un o :

kuse: merde
tute: message
kusotute: shitpost

particules
==========

* er: non
* ok: oui

une particule s'applique au mot qui suit.

ex:

me er ha meme -> je ne suis pas parent

me ha er meme -> je suis un non parant

ha er yanote yan er yanose -> je suis un non-homme et une non-femme

er ha yanote yan yanose -> je ne suis ni un homme ni une femme

er me dasumi tahahe -> ce n'est pas moi qui mangea une pomme (litteralement : "pas-moi mangea une pomme" )

on peut accentuer un mot avec l'affirmation :

ok me zasumi tahahe -> c'est moi qui mangerai une pomme

etc...

Questions
=========

- particule intérogative:

ic

se place avant le mot sur lequel la question porte. 

me ic ha meme? -> suis-je parent?

me ha ic wer? -> où suis-je?

ic yane ha meme? -> Qui est parent?

er yane ha meme. -> personne n'est parent.

possibilité d'oublier la particule si le sens est évident :

me ha wer? -> Je suis où?

- Question sur un choix :

me ha yanose ic kam yanote (ic kam yanome)? -> Suis-je une femme ou un homme (ou non-binaire)?

- particule designatif :

at: cellui-là, ce, cette, lea, l'objet dont il est question,

at tahahe : la pomme (dont je parle)/ cette pomme (que je vois ici).

Les nombres
===========

par defauts, les nombres sont considérés comme des noms :

| Nombres |
|:-------|
|sife: zéro|
|poce: un|
|pire: deux|
|mute: trois|
|mane: quatre|
|cane: cinq|
|nahe: six|
|nowe: sept|
|sere: huit|
|tive: neuf|
|gume: dix|
|zane: cent|
|cure: mille|
|gumopoce: onze|
|pirogume: vingt|
|nahogumotive: soixante-neuf|
|cure (yan) canogume: mille (et) cinquante|

Les jours de la semaine
=======================

(le compte commence à zéro)

| Jours |
|:-----|
|sifodage: Jour nul, qui n'existe pas -> jamais|
|pocodage: lundi|
|pirodage: mardi|
|mutodage: mercredi|
|manodage: jeudi|
|canodage: vendredi|
|nahodage: samedi|
|nowodage: dimanche|

prépositions
============
| prépositions |
|:----|
|toc: par|
|ran: plein de/beaucoup de|
|tun: de (à une relation avec)|
|den: vers (en direction de)|
|cos: à cause de|